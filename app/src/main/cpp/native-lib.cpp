#include <jni.h>
#include <string>

extern "C" JNIEXPORT jstring JNICALL
Java_solorzanomedina_facci_practica24b_PrincipalActivity_stringFromJNI(
        JNIEnv *env,
        jobject /* this */) {
    std::string hello = "Hello from C++";
    return env->NewStringUTF(hello.c_str());
}
