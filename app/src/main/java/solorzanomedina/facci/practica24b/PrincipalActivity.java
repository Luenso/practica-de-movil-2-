package solorzanomedina.facci.practica24b;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class PrincipalActivity extends AppCompatActivity {

    Button buttonLogin, buttonBuscar, buttonGuardar, buttonParametro;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
        buttonLogin = findViewById(R.id.Buttonlogin);
        buttonBuscar = findViewById(R.id.Buttonbuscar);
        buttonGuardar = findViewById(R.id.Buttonguardar);
        buttonParametro = findViewById(R.id.buttonPasarParametros);

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( PrincipalActivity.this, ActivityLogin.class);
                startActivity(intent);
            }
        });
        buttonBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PrincipalActivity.this, ActivityBuscar.class);
                startActivity(intent);
            }
        });
        buttonGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PrincipalActivity.this, ActivityRegistrar.class);
                startActivity(intent);
            }
        });
        buttonParametro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PrincipalActivity.this, ActivityPasarParametros.class);
                startActivity(intent);
            }
        });
    }
}
